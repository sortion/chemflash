#! /usr/bin/env bash
# Generate flash cards (i.e. LaTeX Beamer Slides), two per molecules: one with the name and the other with its structure
INPUT_FILE="$1"
OUTPUT_FILE="$2"

TEMPLATE_CHEMFIG=$(cat "./templates/chemfig.tex.j2")
TEMPLATE_CHEMNAME=$(cat "./templates/chemname.tex.j2")
CHEMICALS=$(cat "$INPUT_FILE" | grep -v "^#" | grep -v "^$")
IFS=$'\n'
for item in $CHEMICALS
do
    CHEMFIG=$(sed -e "s/{{ chemname }}/$item/g" ./templates/chemfig.tex.j2)
    CHEMNAME=$(sed -e "s/{{ chemname }}/$item/g" ./templates/chemname.tex.j2)
    echo "$CHEMNAME" >> "./tmp/slides.tex"
    echo >> "./tmp/slides.tex"
    echo "$CHEMFIG" >> "./tmp/slides.tex"
    echo >> "./tmp/slides.tex"
done

slides="$(cat "./tmp/slides.tex")"
# sed -e "s/{{ slidees }}/$slides/g" "./tmp/slides.tex" > "./tmp/main.tex"