#! /usr/bin/env bash
# Generate all \definesubmol LaTeX/ChemFig in a file
INPUT_FILE="$1"
OUTPUT_FILE="$2"

CHEMICALS=$(cat "$INPUT_FILE" | grep -v "^#" | grep -v "^$")
IFS=$'\n'
for item in $CHEMICALS
do
    chemfig=$(zyme.py -m "$item")
    struct=$(echo "$chemfig" | sed -e 's/\\chemfig{//' -e 's/\}$//g')
    echo "\\definesubmol{${item}}{${struct}}" >> "$OUTPUT_FILE"
    echo >> "$OUTPUT_FILE"
done